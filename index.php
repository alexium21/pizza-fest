<!DOCTYPE html>
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en">  <!--<![endif]-->
<head>
	
	<!-- Google Web Fonts
  ================================================== -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,300italic,400,500%7cCourgette%7cRaleway:200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
	
	<!-- Basic Page Needs
  ================================================== -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>Pizza Fest</title>	
	
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/pizzafest.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">	

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- CSS
  ================================================== -->
    <link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/grid.css" />
	<link rel="stylesheet" href="css/layout.css" />
	<link rel="stylesheet" href="css/fontello.css" />
	<link rel="stylesheet" href="css/animation.css" />
	<link rel="stylesheet" href="css/animate.css" />

	<link rel="stylesheet" href="js/sequence/sequencejs-theme.css" />
	<link rel="stylesheet" href="js/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="css/tooltipster.css" />
	
	
    <link rel="stylesheet" href="js/owl-carousel/owl.carousel.css" />
    <link rel="stylesheet" href="js/owl-carousel/owl.theme.css" />
    <link rel="stylesheet" href="js/owl-carousel/owl.transitions.css" />
	
	<!-- HTML5 Shiv
	================================================== -->
	<script src="js/jquery.modernizr.js"></script>
	
</head>

<body>
	
	
<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

	
<div id="wrapper">
	
	
	<!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->	
	
	
	<nav id="mobile-advanced" class="mobile-advanced"></nav>
	
	
	<!-- - - - - - - - - - - - end Mobile Menu - - - - - - - - - - - - - -->


	<!-- - - - - - - - - - - - - - Top Bar - - - - - - - - - - - - - - - - -->	


	<div id="top-bar" class="color-top">

		<div class="container">

			<div class="row">

				<div class="col-md-7">

					<ul class="mini-contacts">
						<li class="address">Buenos Aires, Argentina</li>
						<li class="email">E-mail: alematute18@gmail.com</li>
					</ul><!--/ .mini-contacts-->

				</div>

				<div class="col-md-5">

					<ul class="social-icons style-fall">
						<li class="linkedin"><a href="https://www.linkedin.com/in/alejandro-matute-504557165/">LinkedIn</a></li>
						<li class="github"><a href="https://bitbucket.org/alexium21/pizza-fest/src">GitHub</a></li>
					</ul><!--/ .social-icons-->

				</div>

			</div><!--/ .row-->

		</div><!--/ .container-->

	</div><!--/ #top-bar-->


	<!-- - - - - - - - - - - - - end Top Bar - - - - - - - - - - - - - - - -->	
	

	<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->	


		


	<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->


	<!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->


	<div id="content">
		<section class="section padding-off">

			<div class="sequence-theme">

				<div id="sequence" class="sequence">
							<div class="full-bg-image animated-element" style="background-image: url(images/pizza1.jpg)"></div>
                            <div class="parallax-overlay animated-element"></div>

							<div class="sequence-entry animated-element">
								<div class="extra-radius bg-yellow">
									<div class="inner-extra">
										<div class="inner-content">

											<h2 class="animate ">Welcome</h2>
											<h2>Trust and Build</h2>
											<h2>Your Pizza!</h2>

										</div>
									</div>
								</div>
							</div>
				</div>
				
			</div>

		</section>

		<div class="container">
			
			
			
			<div class="row">
		
				
		<section class="section padding-off">
		
			<div class="container">
			
			

                <div class="row">
                    <div class="col-xs-12">
                         <br>
                        
                         
                        <div class="col-md-12">
                             <div class="col-md-6">
                             	<h2>The Bacon Cheese</h2>
                             	<img class="alignleft" src="images/pizza2.jpg" alt=""/>
                             </div>
                        	<div class="col-md-6">
                        		<h2>Ingredients:</h2>
	                        	<div class="col-md-7">
	                        	
	                        		<select id="i7" disabled="disabled" class="dropDownIngredients" onchange="setPricing()">
	                        		<option selected="selected">Bacon</option>

	                        		</select>
                        		</div>
	                        	<div class="col-md-3">
	                        		<input type="text"  value="1,5" id="pricing1"><label id="l5">EUR</label>
	                        	</div>
	                        	<div class="col-md-2">
                        			<button class="button default removebtn" id="remove5" style="background-color: #325CFF; display:none" onclick="removeIngredients('i7,pricing1,l5,remove5')"><i class="icon-trash"></i></button>
						
						
                        		</div>
	                        	<div class="col-md-7">
	                        		<select id="i8" disabled="disabled" class="dropDownIngredients">
	                        			<option selected="selected">Mozzarella Cheese</option>
                                    
	                        		</select>	
	                        		</div>
	                        	<div class="col-md-3">
	                        			<input type="text"  value="1,0" id="pricing2"><label id="l6">EUR</label>
							
							
	                        		</div>
	                        		<div class="col-md-2">
                        			<button class="button default removebtn" id="remove6" style="background-color: #325CFF; display:none" onclick="removeIngredients('i8,pricing2,l6,remove6')"><i class="icon-trash"></i></button>
						
						
                        		</div>
	                        	<div class="col-md-7">
	                        		<select id="i9" disabled="disabled" class="dropDownIngredients">
	                        			<option selected="selected">Mushrooms</option>

	                        		</select>	
	                        		</div>
	                        	<div class="col-md-3">
	                        			<input type="text"  value="2,3" id="pricing3"><label id="l7">EUR</label>
							 
							
	                        		</div>
	                        		<div class="col-md-2">
                        			<button class="button default removebtn" id="remove7" style="background-color: #325CFF; display:none" onclick="removeIngredients('i9,pricing3,l7,remove7')"><i class="icon-trash"></i></button>
						
						
                        		</div>
	                        	<div class="col-md-7">
	                        		<select id="i10" disabled="disabled" class="dropDownIngredients">
	                        			<option selected="selected">Oregano</option>
                                       
	                        		</select>	
	                        		</div>
	                        	
	                        	<div class="col-md-3">
	                        			<input type="text"  value="2,4" id="pricing4"><label id="l8">EUR</label>
							
							
	                        		</div>
	                        		<div class="col-md-2">
                        			<button class="button default removebtn" id="remove8" style="background-color: #325CFF; display:none" onclick="removeIngredients('i10,pricing4,l8,remove8')"><i class="icon-trash"></i></button>
						
						
                        		</div>
	                        	
	                        	<div class="col-md-9">
	                        		<label>TOTAL:</label> <input type="decimal" name="" readonly="readonly" id="total" />
	                        		</div>
	                        	
	                            <div class="col-md-9">
	                        		<input type="button" class="button default" name="ingredient" id="ingrech" value="Change Ingredients" style="background-color: #325CFF" onclick="enabledChangeIngredients('i7,i8,i9,i10,remove5,remove6,remove7,remove8')">
	                        	</div>
                        	</div>

                        </div>
                         

                    </div>
                </div>

			</div><!--/ .container-->
					
		</section>
		<section class="section padding-off">
		
			<div class="container">
			
			

                <div class="row">
                    <div class="col-xs-12">
                         <br>
                        
                         
                        <div class="col-md-12">
                             <div class="col-md-6">
                             	<h2>The Fest Veggie</h2>
                             	<img class="alignleft" src="images/champ1.jpg" alt=""/>
                             </div>
                        	<div class="col-md-6">
                        		<h2>Ingredients:</h2>
                        	<div class="col-md-7">
                        	
                        		<select id="i1" disabled="disabled" class="dropDownIngredients">
                        			<option selected="selected">Feta Cheese</option>

                        		</select>	
                        			
                        		
                        		
                        		</div>
                        		<div class="col-md-3">
                        			<input type="text" id="pricing5" value="1,7"><label id="l1">EUR</label>
						 
						
                        		</div>
                        	<div class="col-md-2">
                        			<button class="button default removebtn" id="remove1" style="background-color: #325CFF; display: none;" onclick="removeIngredients('i1,pricing5,l1,remove1')"><i class="icon-trash"></i></button>
						
						
                        		</div>
                        	<div class="col-md-7">
                        		<select id="i2" disabled="disabled" class="dropDownIngredients">
                        			<option selected="selected">Mushrooms</option>

                        		</select>	
                        		</div>
                        	<div class="col-md-3">
                        			<input type="text" id="pricing6" value="2,3"><label id="l2">EUR</label>
						
						
                        		</div>
                        		<div class="col-md-2">
                        			<button class="button default removebtn" id="remove2" style="background-color: #325CFF; display: none;" onclick="removeIngredients('i2,pricing6,l2,remove2')"><i class="icon-trash"></i></button>
						
						
                        		</div>
                        	<div class="col-md-7">
                        		<select id="i3" disabled="disabled" class="dropDownIngredients">
                        			<option selected="selected">Oregano</option>

                        		</select>	
                        		</div>
                        	<div class="col-md-3">
                        			<input type="text" id="pricing7" value="2,4"><label id="l3">EUR</label>
						               
						
                        		</div>
                        		<div class="col-md-2">
                        			<button class="button default removebtn" id="remove3" style="background-color: #325CFF; display: none;" onclick="removeIngredients('i3,pricing7,l3,remove3')"><i class="icon-trash"></i></button>
						
						
                        		</div>
                        	<div class="col-md-7">
                        		<select id="i4" disabled="disabled" class="dropDownIngredients">
                        			<option selected="selected">Tomatoes</option>

                        		</select>	
                        		</div>
                        	<div class="col-md-3">
                        			<input type="text" id="pricing8" value="1,0"><label id="l4">EUR</label>
						
						
                        		</div>
                        	<div class="col-md-2">
                        			<button class="button default removebtn" id="remove4" style="background-color: #325CFF; display:none" onclick="removeIngredients('i4,pricing8,l4,remove4')"><i class="icon-trash"></i></button>
						
						
                        		</div>
                        	
                        	<div class="col-md-9">
                        		<label>TOTAL:</label> <input type="decimal" name="" readonly="readonly" id="total2" />
                        		</div>
                        	
                            <div class="col-md-9">
                        		<input type="button" class="button default" name="ingredient" id="ingrech" value="Change Ingredients" style="background-color: #325CFF" onclick="enabledChangeIngredients('i1,i2,i3,i4,remove1,remove2,remove3,remove4')">
                        	</div>
                        	</div>

                        </div>
                         

                    </div>
                </div>

			</div><!--/ .container-->
					
		</section>
		          
		
				
				
			</div><!--/ .row-->
			
		</div><!--/ .container-->
		<section class="section padding-off color-top">
			
			<div class="container">
				
				<div class="row">
					
					<div class="reading-box">
						
						<div class="col-box-8">
							
						</div>
						
						
						
					</div><!--/ .reading-box-->
						
				</div><!--/ .row-->
				
			</div><!--/ .container-->
			
		</section><!--/ .section-->
		<section class="section" style="background-image:url('images/fontpizza.jpg')">
			
				<div class="col-md-12">
				<div class="col-md-12 align-center">
					<h2><span style="color:#ffffff;">Ingredients</span></h2>
				</div>
				<div class="col-md-6">
					<div class="col-md-6">
						<input type="text" id="ri1" value="Bacon" readonly="readonly" style="background-color: #ffffff;" />
                        <input type="text" id="ri2" value="Feta Cheese" readonly="readonly" style="margin-top: 26px; background-color: #ffffff;" />
				        <input type="text" id="ri3" value="tomatoes" readonly="readonly" style="margin-top: 26px; background-color: #ffffff;" />
					</div>
					<div class="col-md-3">
						<input type="text"  value="1,5" style="background-color: #ffffff;"><label>EUR</label>
						<input type="text"  value="1,7" style="background-color: #ffffff;"><label>EUR</label>
						<input type="text"  value="1,0" style="background-color: #ffffff;"><label>EUR</label>
					</div>
				
				</div>
				<div class="col-md-6">
					<div class="col-md-6">
						<input type="text" id="ri4" value="Mozzarella Cheese" readonly="readonly" style="background-color: #ffffff;"/>
				        <input type="text" id="ri5" value="Mushrooms" readonly="readonly" style="margin-top: 26px; background-color: #ffffff;" />
				        <input type="text" id="ri6" value="Oregano" readonly="readonly" style="margin-top: 26px; background-color: #ffffff;" />
					</div>
					<div class="col-md-3">
						<input type="text" value="1,0" style="background-color: #ffffff;"><label>EUR</label>
						<input type="text" value="2,3" style="background-color: #ffffff;"><label>EUR</label>
						<input type="text" value="2,4" style="background-color: #ffffff;"><label>EUR</label>
					</div>
					
				</div>
				
				
			
			</div>
			
		</section> 
		
		<section class="section padding-off color-top">
			
			<div class="container">
				
				<div class="row">
					
					<div class="reading-box">
						
						<div class="col-box-8">
							
						</div>
						
						
						
					</div><!--/ .reading-box-->
						
				</div><!--/ .row-->
				
			</div><!--/ .container-->
			
		</section><!--/ .section-->
		
		<section class="section bg-gray-color">

			<div class="container">

				<div class="row">

					<div class="col-xs-12">

						<div class="section-title">
							<h2>Contact me</h2>
						</div>

					</div>

				</div><!--/ .row-->

				<div class="row">

					<div class="col-sm-8 col-sm-push-2 col-sm-pull-2">

						

						<center><div class="inputs-block">

								
									<p class="input-block">
								<a class="button default submit pull-center color-top" href="mailto:alematute18@gmail.com" id="submit"><i class="icon-paper-plane-2" style="color: #ffffff; font-size: 20px; font-weight: bold;"></i><br><span style="color:#ffffff; font-weight:bold;">Contact here for any issue or answer.</span></a>  
								</p>
								
								

							</div></center><!--/ .textarea-block-->


						<div class="divider"></div>

						<ul class="social-icons style-fall align-center">
						<li class="linkedin"><a href="https://www.linkedin.com/in/alejandro-matute-504557165/">LinkedIn</a></li>
						<li class="github"><a href="https://bitbucket.org/alexium21/pizza-fest/src">GitHub</a></li>
					</ul><!--/ .social-icons-->

					</div>

				</div><!--/ .row-->

			</div><!--/ .container-->

		</section><!--/ .section-->
		
	</div><!--/ #content-->
	

	<!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->


	<!-- - - - - - - - - - - - - Bottom Footer - - - - - - - - - - - - - - - -->


	<footer class="bottom-footer">

		<div class="container">

			<div class="row">

				<div class="col-xs-12">

					<div class="col-sm-6">

						<div class="copyright">
							Copyright © 2019. <a target="_blank" href="https://www.linkedin.com/in/alejandro-matute-504557165/">Alejandro Matute</a>. All rights reserved
						</div><!--/ .cppyright-->

					</div>

				</div>

			</div><!--/ .row-->

		</div><!--/ .container-->

	</footer><!--/ .bottom-footer-->	


	<!-- - - - - - - - - - - - end Bottom Footer - - - - - - - - - - - - - - -->


</div><!--/ #wrapper-->


<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="js/jquery.sequence-min.js"></script>
<script type="text/javascript" src="js/pizzafest.js"></script>
</body>
</html>
